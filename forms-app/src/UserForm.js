import React, { useState } from "react";

export function UserForm() {
  const [formValues, setFormValues] = useState({
    firstName: "",
    lastName: "",
    age: "",
    employed: false,
    favouriteColor: "",
    sauces: [],
    bestStooge: "Larry",
    notes: "",
  });
  const colors = ["red", "blue", "yellow", "purple", "black"];
  const saucesList = ["Ketchup", "Mustard", "Mayonnaise", "Guacamole"];
  const stooges = ["Larry", "Moe", "Curly"];

  const [formDirty, setFormDirty] = useState(false);

  const validate = (fieldId, regex, maxLength) => {
    const field = document.getElementById(fieldId);
    const value = field.value.trim();
    const isValid = regex.test(value) || value.length <= maxLength;
    field.classList.toggle("invalid", !isValid);
    return isValid;
  };

  const handleChange = (event) => {
    const { name, value, type, checked } = event.target;

    if (name === "sauces") {
      let newSauces = [...formValues.sauces];

      if (checked) {
        newSauces.push(value);
      } else {
        newSauces = newSauces.filter((sauce) => sauce !== value);
      }

      setFormValues({
        ...formValues,
        sauces: newSauces,
      });
    } else {
      setFormValues({
        ...formValues,
        [name]: type === "checkbox" ? checked : value,
      });
    }

    setFormDirty(true);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const firstNameIsValid = validate("firstName", /^[a-zA-Z ]+$/);
    const lastNameIsValid = validate("lastName", /^[a-zA-Z ]+$/);
    const ageIsValid = validate("age", /^[0-9]+$/);
    const notesIsValid = validate("notes", /^[\s\S]{0,100}$/, 100);

    if (firstNameIsValid && lastNameIsValid && ageIsValid && notesIsValid)
      alert(JSON.stringify(formValues, null, 2));
  };

  const handleReset = (event) => {
    event.preventDefault();
    setFormValues({
      firstName: "",
      lastName: "",
      age: "",
      employed: false,
      favouriteColor: "",
      sauces: [],
      bestStooge: "Larry",
      notes: "",
    });

    setFormDirty(false);
  };

  const isSubmitDisabled = !formDirty;
  const isResetDisabled = !formDirty;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="firstName">First Name</label>
        <input
          type="text"
          id="firstName"
          name="firstName"
          placeholder="First Name"
          value={formValues.firstName}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="lastName">Last Name </label>
        <input
          type="text"
          id="lastName"
          name="lastName"
          placeholder="Last Name"
          value={formValues.lastName}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="age">Age</label>
        <input
          type="text"
          id="age"
          name="age"
          placeholder="Age"
          value={formValues.age}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="employed">Employed</label>
        <input
          type="checkbox"
          id="employed"
          name="employed"
          checked={formValues.employed}
          onChange={handleChange}
        />
      </div>
      <div>
        <label htmlFor="favouriteColor">Favourite Color</label>
        <select
          id="favouriteColor"
          name="favouriteColor"
          value={formValues.favouriteColor}
          onChange={handleChange}
        >
          <option />
          {colors.map((color) => {
            return <option key={color}>{color}</option>;
          })}
        </select>
      </div>
      <div>
        <label>Sauces</label>
        <div className="sauces-list">
          {saucesList.map((sauce) => {
            return (
              <div key={sauce}>
                <input
                  id={sauce}
                  type="checkbox"
                  name="sauces"
                  value={sauce}
                  checked={formValues.sauces.includes(sauce)}
                  onChange={handleChange}
                />
                <label htmlFor={sauce}>{sauce}</label>
              </div>
            );
          })}
        </div>
      </div>
      <div>
        <label>Best Stooge</label>
        <div className="stooges-list">
          {stooges.map((stooge) => {
            return (
              <div key={stooge}>
                <input
                  type="radio"
                  id={stooge}
                  name="bestStooge"
                  value={stooge}
                  checked={formValues.bestStooge === stooge}
                  onChange={handleChange}
                />
                <label htmlFor={stooge}>{stooge}</label>
              </div>
            );
          })}
        </div>
      </div>
      <div>
        <label htmlFor="notes">Notes</label>
        <textarea
          id="notes"
          name="notes"
          value={formValues.notes}
          onChange={handleChange}
          placeholder="Notes"
        ></textarea>
      </div>
      <div className="buttons">
        <button type="submit" disabled={isSubmitDisabled}>
          Submit
        </button>
        <button type="button" onClick={handleReset} disabled={isResetDisabled}>
          Reset
        </button>
      </div>
      <pre>{JSON.stringify(formValues, null, 2)}</pre>
    </form>
  );
}
